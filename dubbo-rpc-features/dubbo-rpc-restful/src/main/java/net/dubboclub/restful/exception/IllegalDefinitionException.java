package net.dubboclub.restful.exception;

import net.dubboclub.restful.modal.RestfulPackage;

import java.lang.reflect.Method;

/**
 * Created by bieber on 16/3/10.
 */
public class IllegalDefinitionException extends IllegalStateException {

    private String message;

    public IllegalDefinitionException(Class serviceType,Method method){
        Class[] types = method.getParameterTypes();
        StringBuffer stringBuffer = new StringBuffer(serviceType.getName());
        stringBuffer.append(".").append(method.getName()).append("(");
        for(Class type:types){
            stringBuffer.append(type.getName()).append(",");
        }
        if(types.length>0){
            stringBuffer.setLength(stringBuffer.length()-1);
        }
        stringBuffer.append(")").append(" incorrect definition.Method argument must be class "+ RestfulPackage.class+" class or sub class.");
        this.message=stringBuffer.toString();
    }


    @Override
    public String getMessage() {
        return message;
    }
}
