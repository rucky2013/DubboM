package net.dubboclub.http.netty.servlet;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.multipart.HttpData;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * @date: 2016/3/8.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.servlet.
 * @version:1.0.0
 * @fix:
 * @description: 描述功能
 */
public class NettyHttpServletPart implements Part {

    private HttpData httpData;

    private InputStream inputStream;

    private long size;

    public NettyHttpServletPart(HttpData fileUpload) throws IOException {
        this.httpData = fileUpload;
        try {
            this.size = fileUpload.getByteBuf().readableBytes();
            ByteBuf content = fileUpload.getByteBuf();
            content.retain();
            this.inputStream = new ByteBufInputStream(content);
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return inputStream;
    }

    @Override
    public String getContentType() {
        return httpData.getHttpDataType().name();
    }

    @Override
    public String getName() {
        return httpData.getName();
    }

    @Override
    public long getSize() {
        return size;
    }

    @Override
    public void write(String fileName) throws IOException {
        throw new UnsupportedOperationException("unsupport write.");
    }

    @Override
    public void delete() throws IOException {
        throw new UnsupportedOperationException("unsupport delete.");
    }

    @Override
    public String getHeader(String name) {
        throw new UnsupportedOperationException("unsupport getHeader.");
    }

    @Override
    public Collection<String> getHeaders(String name) {
        throw new UnsupportedOperationException("unsupport getHeaders.");
    }

    @Override
    public Collection<String> getHeaderNames() {
        throw new UnsupportedOperationException("unsupport getHeaderNames.");
    }
}
