package net.dubboclub.http.netty.servlet;

import io.netty.buffer.ByteBuf;

import javax.servlet.ServletOutputStream;
import java.io.IOException;

/**
 * @date: 2016/3/8.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.servlet.
 * @version:1.0.0
 * @fix:
 * @description: netty的http输出流
 */
public class NettyHttpServletOutputStream extends ServletOutputStream {

    private ByteBuf content;

    public NettyHttpServletOutputStream(ByteBuf content) {
        this.content = content;
    }



    @Override
    public void write(int b) throws IOException {
        content.writeByte(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        content.writeBytes(b,off,len);
    }

    @Override
    public void write(byte[] b) throws IOException {
        write(b,0,b.length);
    }
}
