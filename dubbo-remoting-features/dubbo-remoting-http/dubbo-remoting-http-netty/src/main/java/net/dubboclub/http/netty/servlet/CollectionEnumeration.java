package net.dubboclub.http.netty.servlet;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * @date: 2016/3/7.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.servlet.
 * @version:1.0.0
 * @fix:
 * @description: 描述功能
 */
public class CollectionEnumeration<T extends Object> implements Enumeration<T> {

    private Iterator<T> values;

    public CollectionEnumeration(Collection<T> values) {
        this.values = values.iterator();
    }

    @Override
    public boolean hasMoreElements() {
        return values.hasNext();
    }

    @Override
    public T nextElement() {
        return values.next();
    }
}
