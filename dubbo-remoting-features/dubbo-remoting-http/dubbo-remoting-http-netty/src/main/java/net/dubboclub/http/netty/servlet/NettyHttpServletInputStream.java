package net.dubboclub.http.netty.servlet;

import io.netty.buffer.ByteBuf;

import javax.servlet.ServletInputStream;
import java.io.IOException;

/**
 * @date: 2016/3/7.
 * @author:bieber.
 * @project:dubbo-side.
 * @package:com.alibaba.dubbo.remoting.http.netty.servlet.
 * @version:1.0.0
 * @fix:
 * @description: netty的http服务端的请求输入流
 */
public class NettyHttpServletInputStream extends ServletInputStream {

    private ByteBufInputStream byteBufInputStream;

    public NettyHttpServletInputStream(ByteBuf byteBuf) throws IOException {
        byteBufInputStream = new ByteBufInputStream(byteBuf);
    }

    @Override
    public int read() throws IOException {
        return byteBufInputStream.read();
    }

    @Override
    public int readLine(byte[] b, int off, int len) throws IOException {
        return byteBufInputStream.read(b,off,len);
    }
}
