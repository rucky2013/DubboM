#DubboM——Dubbo Micro

##目的
通过利用Dubbo的扩展性以及RPC的模型开发一个满足微服务的要求，通过构建一个RPC的环境，让应用在不依赖外部第三方容器能够进行服务
的发布和调用，从而打到应用的隔离以及完整性。应用应该只需要关注与它的服务实现以及服务的调用，而不需要关注服务在处于什么样的一个
环境，基于这个便构建了DubboM(Dubbo Micro)。
##项目获取
如果你还不熟悉git，或者不知道git是什么，请百度，根据相关文档完成git安装以及基本使用

通过一下命令获取`DubboM`源码：

`git clone https://git.oschina.net/bieber/DubboM.git`

##构建项目

此处假设你通过上面的`git`命令把源码下载到本地的`/git/DubboM`下（后面均以`${DubboM_Source_Home}`）.

`cd ${DubboM_Source_Home}`到该目录下面,执行如下目录

`./install-${rpc_type}.bat(sh)`其中`${rpc_type}`分别是`dubbo`和`restful`，表示当前支持两种协议，一种是`dubbo`的二进制长连接
协议，另一种是`restful`的基于http的短连接协议

执行完上面脚本之后，在`${DubboM_Source_Home}/target`下面会有`DubboM-launcher`目录，该目录下有如下几个文件夹：

`bin`:启动该DubboM-launcher脚本

`conf`:该应用相关的配置，会加载到`classpath`下面

`libs`:DubboM依赖的相关jar文件

`apps`:该DubboM-launcher负责启动的dubbom应用，将该应用的jar以及相关依赖的jar添加到该目录下

###什么是DubboM-launcher

你可能会问什么是`DubboM-launcher`,`DubboM-launcher`可以理解为是一个DubboM应用的启动器，可以简单理解为和servlet容器的tomcat或者jetty
类似，但是唯一不同的是tomcat可以管理多个应用实体，但是`DubboM-launcher`有且只能有一个应用，只要你把你的应用jar拷贝到apps目录下，你可以把`DubboM-launcer`
拷贝到任何地方，任何物理节点，或者虚机或者是docker，因为它已经具备了该应用运行的完整环境。

##搭建DubboM项目


##对DubboM项目进行打包


##对DubboM项目开发调试

##对DubboM项目发布部署
