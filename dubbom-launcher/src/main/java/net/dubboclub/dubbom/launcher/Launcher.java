package net.dubboclub.dubbom.launcher;

import com.alibaba.dubbo.common.utils.ConfigUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @date: 2016/3/11.
 * @author:bieber.
 * @project:DubboM.
 * @package:net.dubboclub.dubbom.launcher.
 * @version:1.0.0
 * @fix:
 * @description: DubboM启动器
 */
public class Launcher {

    private static volatile boolean running=true;

    private static volatile ClassPathXmlApplicationContext applicationContext;

    public static void main(String[] args){
        if(args.length<=0){
            throw new IllegalArgumentException("args must not be null.");
        }
        String command = args[0];
        if("start".equalsIgnoreCase(command)){
            start();
        }else if("stop".equalsIgnoreCase(command)){
            stop();
        }else{
            throw new IllegalArgumentException("Unknow "+command+" argument.");
        }

    }

    public static void start(){
        String springXMLPath = ConfigUtils.getProperty("dubbom.spring.xml","classpath*:META-INF/spring/*.xml");
        String[] paths = StringUtils.split(springXMLPath,",");
        applicationContext = new ClassPathXmlApplicationContext(paths);
        applicationContext.start();
        synchronized (Launcher.class) {
            while (running) {
                try {
                    Launcher.class.wait();
                } catch (Throwable e) {
                }
            }
        }
    }


    public static void stop(){

    }
}
